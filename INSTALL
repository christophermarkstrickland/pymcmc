INSTALL instructions 
====================

Pymcmc should build relatively easily under most linux/
unix platforms. For example, under ubuntu, to prepare,
running the commands:

sudo apt-get install python-scipy libatlas3gf-sse2
sudo python setup.py install

should be all you need to do. 

For other platforms, we recommend you install the binary versions,
but if you need to build from source, see the instructions later
in this file.

Requirements:
=============
*Python*
Pymcmc was built using Python 2.6. You will also need
the following python packages:numpy, scipy, matplotlib.
Each of these should be relatively easy to install under
most platforms. See the web pages for more information.

*ATLAS*
Some functions require blas/lapack. We recommend installing
atlas for your platform to get the best performance. Most
distributions will have precompiled versions available, 
but if you need to build your own, there are numerous web
pages with instructions. See, for example, the scipy wiki
at http://www.scipy.org/Installing_SciPy/Linux

If you install the windows or Mac binary of pymcmc, you 
will not need to install atlas unless you want to create
new functions using fortran or f2py which require ATLAS.
See the windows build instructions further down for information
on building ATLAS on windows.

*optional packages*
The python package pysparse (http://pysparse.sourceforge.net/)
is used in  one of the examples.
If you prefer to analyse the posterior samples in R, you will
need to install R. Rpy2 allows you to call R from within python,
and can be useful.


INSTALLING
==========
Most users should just need to run:
python setup.py install

If your ATLAS libraries are in places that aren't detected
by python, you may have to modify setup.py. See the comments
in that file for more information.

WINDOWS USERS
=============
The easiest way to get started using pymcmc is to install
python2.7, numpy, scipy, and matplotlib using windows binaries
(see the websites of each of these for information on this).
Then install the windows binary of pymcmc. If you need to
build pymcmc from source, here are some guidelines:

*building from source under windows*
Install python, numpy,scipy,matplotlib. 

For building ATLAS,
look at http://www.scipy.org/Installing_SciPy/Windows, and
follow http://nipy.sourceforge.net/nipy/stable/devel/install/windows_scipy_build.html 
and also http://www.mingw.org/wiki/msys/.
I installed 1.0.11. The DTK provides everything you need (bzip2, 
diffutils, gawk,make,binutils)

Install cygwin, and build atlas from there.


Make sure you can compile a simple blas progam, using something like:

cat > dgemmeg.f << "EOF"
      REAL*8 X(2, 3) /1.D0, 2.D0, 3.D0, 4.D0, 5.D0, 6.D0/
      REAL*8 Y(3) /2.D0, 2.D0, 2.D0/
      REAL*8 Z(2)
      CALL DGEMV('N', 2, 3, 1.D0, X, 2, Y, 1, 0.D0, Z, 1)
      PRINT *, Z
      STOP
      END
EOF

gfortran dgemmeg.f -L /d/tmp/pymcmc_win_install/ATLAS/cygwin_build/install/lib/ -lf77blas -latlas 

Make sure f2py works, try instructions on

http://www.scipy.org/F2PY_Windows

cat > test.f90 << "EOF"
subroutine hello ()
    write(*,*)'Hello from Fortran90!!!'
end subroutine hello
EOF


/c/Python27/python /c/Python27/Scripts/f2py.py -c -m foo test.f90



##now try one which uses the blas example
cat > blas_eg.f90 << "EOF"
subroutine dgemveg()
  REAL*8 X(2, 3) /1.D0, 2.D0, 3.D0, 4.D0, 5.D0, 6.D0/
  REAL*8 Y(3) /2.D0, 2.D0, 2.D0/
  REAL*8 Z(2)
  CALL DGEMV('N', 2, 3, 1.D0, X, 2, Y, 1, 0.D0, Z, 1)
  PRINT *, Z
end subroutine dgemveg
EOF


/c/Python27/python /c/Python27/Scripts/f2py.py -c -m foo blas_eg.f90 -L"/d/tmp/pymcmc_win_install/ATLAS/cygwin_build/install/lib/" -lf77blas -latlas

Unpack the pymcmc archive, and modify the setup.py file to make sure 
the library_dirs are set correctly for your system, for example
library_dirs = ["/d/tmp/pymcmc_win_install/BUILDS/lib/"], but see
the instructions in the setup.py file.

Run
 python setup.py build
then 

python setup.py bdist

or 

python setup.py bdist --format=wininst


MAC Users:
=============
The easiest way to get started using pymcmc is to install
python2.6, numpy, scipy, matplotlib using mac binaries
(see the websites of each of these for information on this).
Then install the macintosh binary of pymcmc. This is a simple
zip file, and you should unzip it and move the archive to the
appropriate place. If you unzip it from / it will install into
./Library/Frameworks/Python.framework/Versions/2.6/lib/python2.6/site-packages.

*building from source*
If you need to build pymcmc from source, here are some guidelines:

Get a recent version of python, I used python2.6. It is generally
recommended to not use the mac inbuilt version of python.  Then follow
the instructions to get scipy installed, including getting developer
tools. I'm using 10.5.8, so I downloaded 3.1.3 (994.6MB!)

You also need a fortran compiler, I got gfortran after following
links on building R for mac (http://r.research.att.com/tools/).

I couldn't get gfortran to link without using the dynamic version of
libgfortran. In the end, I temporarily moved the dynamic libs and
created a link from libgfortran.a to libgfortran_static.a and put libs
= ['libgfortran_static'] in setup.py

Then, 
python setup.py bdist
and distribute.


Building DEB package
====================

Under debian based systems, it is possible to build a deb package
relatively simply by using the python stdeb package (see 
http://pypi.python.org/pypi/stdeb).
python setup.py --command-packages=stdeb.command sdist_dsc
This will create a source package (.dsc, .orig.tar.gz and .diff.gz files).
To make a binary package:
cd deb_dist/pymcmc-1.0
dpkg-buildpackage -rfakeroot -uc -us
This will leave a binary deb in deb_dist
Some options for the deb can be set in the stdeb.cfg file.

My current approach, is as follows:

VERSION=1.1a
DEBVERSION=1
python setup.py sdist
cd dist
py2dsc -x ../stdeb.cfg pymcmc-$VERSION.tar.gz
cd deb_dist/pymcmc-1.1a

edit debian/rules so that it includes the following lines:

LDFLAGS += -shared
CFLAGS += -fPIC
FFLAGS += -fPIC

dpkg-buildpackage -rfakeroot -uc -us

Hopefully should build correctly.
To sign and upload the source package:

debuild -S -sa
cd ..
dput ppa:rjadenham/ppa pymcmc_$VERSION-$DEBVERSION_source.changes

Using pbuilder to check dependencies.
This is my pbuildrc at work:

COMPONENTS="main restricted universe multiverse"
export http_proxy="http://gw-esp-proxy.dmz:80"


sudo pbuilder build deb_dist/pymcmc_${VERSION}-${DEBVERSION}.dsc

Dependencies:
sudo apt-get install python-numpy libatlas-base-dev build-essential gfortran python-dev
sudo apt-get install python-stdeb python-all-dev




