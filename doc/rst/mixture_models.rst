MixtureModel
============

The finite univariate Normal mixture model can be represented by the  likelihood;

.. math:: g(\mathbf{y}) \approx \hat{g}(\mathbf{y}) = \prod_{i=1}^N \sum^k_{j=1}\lambda_j \frac{1}{\sqrt{2\pi\sigma_j^2}}\exp\left[-\frac{1}{2}\left(\frac{y_i - \mu_j}{\sigma_j}\right)^2\right] 

where :math:`N` is the sample size,  :math:`k` is the number of components in the mixture, :math:`\mu_j` and :math:`\sigma_j^2` are the mean and variance of component :math:`j` and :math:`\lambda_j` represents the weight of component :math:`j`, that is, the probability that the data is classified into the j-th component.

The Bayesian estimation usually takes a hierarchical framework. This is achieved by introducing a new variable, :math:`z_i`, which is a :math:`(1 \times k)` indicator vector which represents the unobserved component membership of the data. The variable :math:`z_i` has a sampling distribution, given the weights, :math:`\lambda`, which is taken to be a Multinomial distribution. Then, the joint conditional distribution of the data (:math:`y_i`) and the unobserved component membership (:math:`z_i`) is given by;

.. math::
   :nowrap:

   \begin{eqnarray*} 
    p(y,z\mid\mu,\sigma^2,\lambda) & \propto & p(z\mid\lambda)p(y\mid\mu,\sigma^2,z)  \\
     & \propto & \prod^N_{i=1}\prod^k_{j=1}\left\{\lambda_j \left(\sqrt{2\pi\sigma_j^2}\right)^{-1} \exp\left[-\frac{1}{2}\left(\frac{y_i-\mu_j}{\sigma_j}\right)^2\right]\right\}^{z_{ij}}
    \end{eqnarray*}


where :math:`z_{ij} = 1` if :math:`y_i` belongs to component :math:`j`, otherwise :math:`z_{ij} = 0`.

Prior distributions
-------------------

To analyse the mixture model using the Gibbs sampler we need to nominate suitable prior distributions. The standard models library uses conjugate priors, with two options for the component mean, as given below. The values on the right hand side are the default hyperparamters if none are specified by the user.

.. math::
  :nowrap:

  \begin{eqnarray*}  
   \boldsymbol{\lambda} & \sim \mbox{Dirichlet}(\alpha_1,\cdots,\alpha_k) & \mbox{Dirichlet}(1,\cdots,1) \\
   & & \\
   \sigma_j^2 & \sim  \mbox{IG}\left(\frac{\nu_j}{2},\frac{s_j^2}{2}\right) & \mbox{IG}(1, 0.1) \\
   & & \\
   \mu_j\mid\sigma_j^2 & \sim  \mbox{N}\left(\xi_j,\frac{\sigma_j^2}{m_j}\right) & \mbox{N}\left(\bar{y},\frac{\sigma_j^2}{5}\right)  \\
   \mbox{OR} & &  \\
   \mu_j & \sim \mbox{N}\left(\xi_j,\varepsilon_j\right) & \mbox{not default} 
   \end{eqnarray*} 

where :math:`\alpha_j, \xi_j, m_j, \varepsilon_j, \nu_j` and :math:`s_j` are fixed hyperparameters. 

Estimation is acheived using a two stage Gibbs sampler. Stage 1 consists of updating the unknown component allocations :math:`z` and calculating required quantities for use in stage 2. Then stage 2 involves updating estimates for the componet means, variances and weights.

Following is example code for the duration of eruptions in minutes at the Old Faithful Geyser which using default values for all options
::

    # Load libraries

    import numpy as np
    import os
    import pymcmc
    from pymcmc.standard.mix_model import MixtureModel

    # import data
    datadir = os.path.join(os.path.dirname(pymcmc.__file__),'data')
    datamat = np.loadtxt(os.path.join(datadir,'OldFaithful.txt'))

    np.random.seed(12345)
    numits = 40000
    burnin = 10000
    numcomps = 2

    outmix = MixtureModel(numits,burnin,datamat[:,0],numcomps, runtime_output = True)
    outmix.output(parameters=['alpha','sigma','p'])

The ouput for this model is

::

    --------------------------------------------------------

    PyMCMC is now running

    The number of iterations = 40000
    The length of the burnin = 10000

    [########################################]

    -------------------------------------------------------

    The time (seconds) for the MCMC sampler = 15.33
    Number of blocks in MCMC sampler =  3

                        mean          sd         2.5%       97.5%      IFactor
        alpha[0]        2.12      0.0425         2.04        2.21         3.51
        alpha[1]        4.28      0.0316         4.21        4.34         3.47
        sigma[0]       0.413      0.0316        0.354       0.477         3.82
        sigma[1]       0.412      0.0228        0.369       0.457         3.52
            p[0]       0.362      0.0292        0.306       0.419         3.47
            p[1]       0.638      0.0292        0.581       0.694         3.47

    Acceptance rate  alpha  =  1.0
    Acceptance rate  sigma  =  1.0
    Acceptance rate  p  =  1.0

This is an example of the same mixture model on the alternative measure in this data set, the waiting time in minutes between eruptions, however, the start values and hyperparameters are user fixed

::

    # Same model using options for start values and prior hyperparameters
    
    initsigma = np.ones(numcomps) * 1
    initmu = np.array([50.0, 80.0])
    
    priormeanbar =  np.ones(numcomps)*np.mean(datamat[:,1])
    priormeanss = np.ones(numcomps)*10
    priorvarp1 = 2.0
    priorvarp2 = 7.9
    priormean = ['normal_inverted_gamma', priorvarp1, priorvarp2, priormeanbar, priormeanss]

    mixture = MixtureModel(numits, burnin, datamat[:,1], numcomps, prior_mean_sigma = priormean, initial_sigma = initsigma, initial_alpha = initmu, runtime_output = True)
    mixture.output(parameters=['alpha','sigma','p'])

The output is as follows

::

    --------------------------------------------------------

    The time (seconds) for the MCMC sampler = 15.35
    Number of blocks in MCMC sampler =  3

                    mean          sd         2.5%       97.5%      IFactor
    alpha[0]          56       0.828         54.4        57.7         4.99
    alpha[1]        80.1       0.484         79.1          81         4.08
    sigma[0]        7.04       0.625         5.85        8.25         5.46
    sigma[1]        5.73        0.36         5.04        6.44         4.33
        p[0]       0.378      0.0321        0.314       0.439            4
        p[1]       0.622      0.0321        0.561       0.686            4

    Acceptance rate  alpha  =  1.0
    Acceptance rate  sigma  =  1.0
    Acceptance rate  p  =  1.0



Multivariate Normal mixtures
----------------------------

This data set is also amenable to a Multivariate mixture analysis. The code using defaults for the multivariate analysis of the Old Faithful data set is given below. It is necessary to standardise the data in this example

::

    n,k = datamat.shape
    std_data=np.zeros((n,k))
    std_data[:,0]=(datamat[:,0]-np.mean(datamat[:,0]))/np.std(datamat[:,0])
    std_data[:,1]=(datamat[:,1]-np.mean(datamat[:,1]))/np.std(datamat[:,1])

    mixture = MixtureModel(numits, burnin, std_data, numcomps,runtime_output=True)
    mixture.output(parameters = ['alpha', 'sigma', 'p'])

The output is as follows

::

    --------------------------------------------------------
    The time (seconds) for the MCMC sampler = 39.31
    Number of blocks in MCMC sampler =  3

                        mean          sd         2.5%       97.5%      IFactor
     alpha[0, 0]       -1.19      0.0412        -1.27       -1.11         3.56
     alpha[0, 1]       0.694      0.0311        0.638       0.753         3.44
     alpha[1, 0]       -1.13      0.0567        -1.25       -1.02         3.43
     alpha[1, 1]       0.661      0.0377        0.592       0.734         3.44
  sigma[0, 0, 0]        6.56       0.963         4.79        8.54         3.55
  sigma[0, 0, 1]        7.04       0.753         5.57        8.51         3.48
  sigma[0, 1, 0]       -1.88       0.675        -3.21      -0.559         3.43
  sigma[0, 1, 1]       -1.47       0.554        -2.53      -0.369         3.51
  sigma[1, 0, 0]       -1.88       0.675        -3.21      -0.559         3.43
  sigma[1, 0, 1]       -1.47       0.554        -2.53      -0.369         3.51
  sigma[1, 1, 0]        4.11       0.677         2.84        5.46         3.46
  sigma[1, 1, 1]        5.05       0.586         3.91         6.2         3.45
            p[0]       0.365      0.0294        0.309       0.423         3.48
            p[1]       0.635      0.0294        0.577       0.691         3.48

    Acceptance rate  alpha  =  1.0
    Acceptance rate  sigma  =  1.0
    Acceptance rate  p  =  1.0

