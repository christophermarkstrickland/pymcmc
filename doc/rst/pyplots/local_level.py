#import libraries
import numpy as np
from pyssm.ssm import SimSmoother
from pymcmc.regtools import CondScaleSampler
from pymcmc.mcmc import MCMC, CFsampler
import pymcmc
import os
import pylab as plt

def simstate(store):
    "Function simulates state."
    return store['simsm'].sim_smoother()

def simsigsqeps(store):
    "Simulates scale for observation equation"
    res = store['simsm'].get_meas_residual().flatten()
    sigsqeps = store['scale_sampler'].sample(res) ** 2 # sample scale
    system = store['simsm'].get_system() #class that store system matricies
    system.update_ht(sigsqeps) # update variance parameter for measurement equation.
    return sigsqeps

def simsigsqeta(store):
    "Simulates scale from state equation."
    res = store['simsm'].get_state_residual().flatten()
    sigsqeta = store['scale_sampler'].sample(res) ** 2 #sample scale
    system = store['simsm'].get_system()
    system.update_qt(sigsqeta)# update variance parameter for state equation

    return sigsqeta

def main():
    "Main function"

    np.random.seed(12345) #Set seed for random number generator.

    #load data Nile example from Durbin and Koopman (2001) Book
    datadir = os.path.join(os.path.dirname(pymcmc.__file__),'data')
    yvec = np.loadtxt(os.path.join(datadir,'Nile.dat'), skiprows = 1) / 100.

    #define data dictionary, which all functions called from MCMC engine
    #will have access to.
    data = {'yvec': yvec}

    #Define class instance used to simulate state
    data['simsm'] = SimSmoother(yvec, 1, 1, False)

    #Initial values for system matrices
    zt = 1.0; tt = 1.; gt = 1.; rt = 1.
    ht = 0.06 #Error variance in the observation equation
    qt = 0.5  #Error variance in the state equation
    a1 = 0.; p1 = 1000# prior on initial state

    data['simsm'].initialise_system(a1, p1, zt, ht, tt, gt, qt, rt)

    #define class for CondScaleSampler with Jeffreys prior
    data['scale_sampler'] = CondScaleSampler()

    #define block to sample state (note flag to not store iterates)
    #Also first in blocking scheme so don't need inteligent starting values
    sample_state = CFsampler(simstate, np.zeros((1, yvec.shape[0])), 'state', store = 'none')

    #define blocks for sampling variance for measurement and state equations
    sample_ht = CFsampler(simsigsqeps, 1., 'ht')
    sample_qt = CFsampler(simsigsqeta, 1., 'qt')

    #Define MCMC sampling scheme to run for 20000 iterations with the first 5000 iterations discarded
    mcmc = MCMC(20000, 5000, data, [sample_state, sample_ht, sample_qt],
                runtime_output = True)
    mcmc.sampler()
    mcmc.output(parameters = ['ht', 'qt'])

    #plot data versus level (mstate is posterior mean for state)
    mstate, vstate = mcmc.get_mean_var('state')

    plt.plot(mstate.flatten())
    plt.plot(yvec)
    plt.title("Nile data set")
    plt.show()



#Call main function
main()
