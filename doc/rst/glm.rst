Generalised Linear Models
=========================

Generalised linear models (GLM)s are a generalisation of the normal linear model to distributions
from the exponential families. Currently, PyMCMC, supports GLMs from the normal distribution 
(i.e. linear model), poisson and Binomial families.

Normal family
------------
See Section :doc:`linear_regression`.

Binomial family
--------------

The :class:`GLM` class in PyMCMC can be used to estimate GLMs from the binomial family of
distributions. In paricular, the logitic link function can be used for the general binomial
case, which the Probit link function can only be currently used for the special case
of the Bernoulli distribution.

Probit Regression Example
^^^^^^^^^^^^^^^^^^^^^^^^^
The following code presents an example of the Probit model for Bernouli data.

::

    #import libraries
    import numpy as np
    from pymcmc.standard.glm import GLM

    def simdata(N, kreg):
        """function simulates Bernoulli data for probit model"""
        beta = np.random.randn(kreg)
        xmat = np.hstack([np.ones((N, 1)), np.random.randn(N, kreg-1)])
        sigma = 1.0
        yvec = np.zeros(N, dtype = 'i')
        ystar = np.dot(xmat, beta) + sigma * np.random.randn(N)
        yvec[ystar >= 0] = 1
        yvec[ystar < 0] = 0

        return yvec, xmat

    #Main program starts here

    def main():
        "Main function."

        np.random.seed(12345) #Seed random number generator
        nobs = 1000 #Number of observations
        kreg = 8 #Number of regressors

        #Simulate data
        yvec, xmat = simdata(nobs, kreg)

        #Define class for Probit model
        probit = GLM(yvec, xmat, 'binomial-probit', runtime_output = True)
        probit.output(parameters = 'beta') #Produce output

    #Run main program
    main()

The output for the Probit model is as follows:

::

    The time (seconds) for the MCMC sampler = 5.54
    Number of blocks in MCMC sampler =  2

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]      -0.229      0.0619       -0.346      -0.106         9.02
         beta[1]        0.37       0.063        0.246       0.494         10.5
         beta[2]      -0.415      0.0595       -0.534      -0.299         9.96
         beta[3]      -0.447      0.0657        -0.58      -0.325         10.7
         beta[4]        1.83       0.107         1.61        2.03         29.4
         beta[5]        1.27       0.088         1.09        1.43         22.4
         beta[6]       0.222      0.0627       0.0963       0.339         8.81
         beta[7]       0.249       0.063        0.125        0.37         9.39

    Acceptance rate  beta  =  1.0
    BIC =  615.124
    Log likelihood = -279.931


Logistic Regression Example
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Poisson Regression Example
^^^^^^^^^^^^^^^^^^^^^^^^^^

The following code presents an example of 


::

    #import libraries
    import numpy as np
    from pymcmc.standard.glm import GLM

    def simdata(nobs, kreg):
        """function siulates count data from the log-linear model"""

        beta = np.random.randn(kreg)/ 10
        xmat = np.hstack([np.ones((nobs, 1)), np.random.randn(nobs, kreg - 1)])

        lamb = np.exp(np.dot(xmat, beta))
        yvec = np.random.poisson(lamb)
        return yvec, xmat

    def main():
        "Main function"
        np.random.seed(12345) #Random seed
        nobs = 1000 #Number of observations
        kreg = 4 #Number of regressors

        #Simulate data
        yvec, xmat = simdata(nobs, kreg)

        #Define class for Poisson regression
        loglinear = GLM(yvec, xmat, 'poisson', runtime_output = True)
        loglinear.output()

    #Call main function
    main()

Output is as follows:

::

    The time (seconds) for the MCMC sampler = 8.49
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]     -0.0471      0.0233      -0.0909    -0.00202          7.9
         beta[1]      0.0905      0.0239       0.0429       0.137         8.63
         beta[2]     -0.0593      0.0232       -0.107     -0.0149         7.73
         beta[3]     -0.0375      0.0239      -0.0838      0.0112         8.07

    Acceptance rate  beta  =  0.573666666667
    BIC =  2013.779
    Log likelihood = -993.074

