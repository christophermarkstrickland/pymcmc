.. _RWMH:
Random Walk Metropolis Hastings
==============================

The random walk Metropolis Hastings (RWMH), see [RobertCassela1999]_, is a particularly popular algorithm in Markov chain Monte Carlo (MCMC) analysis.
Suppose we are interested in analysing the posterior distribution :math:`\pi\left(\boldsymbol{\theta}\right)`, where 
:math:`\boldsymbol{\theta}` is a :math:`\left(k \times 1\right)` vector of unknown parameters, then the
RWMH algorithm can be used to produce an ergodic Markov chain with stationary distribution :math:`\pi`. To
do this the algorithm uses a random walk proposal distribution, such that a candidate
:math:`\boldsymbol{\theta}^*`  is proposed, at iteration :math:`i+1` where 

.. math:: \boldsymbol{\theta}^*=\boldsymbol{\theta}^{(i)}+\boldsymbol{\varepsilon}_i, 
where :math:`\boldsymbol{\varepsilon}_i\sim N\left(\mathbf{0}, \boldsymbol{\Sigma}\right).` The success of the 
RWMH depends greatly on the choice of :math:`\boldsymbol{\Sigma}`, which becomes increasingly difficult as 
:math:`k` increases. PyMCMC facilitates the use of the RWMH algoritm through the class :class:`RWMH`.
Like all of the algorithms in PyMCMC their use is enabled though the class :class:`MCMC`, which is
the main driver for MCMC algorithms. As an example consider the Bernoulli Logit regression model, where
for an :math:`\left(n \times 1\right)` vector of observations :math:`\mathbf{y}` the likelihood function
is given by

.. math:: p\left(\mathbf{y}\right)=\prod_{i=1}^n\boldsymbol{\lambda}_i^{y_i}\left(1-\boldsymbol{\lambda}_i\right)^{1-y_i},
where :math:`\boldsymbol{\lambda}_i=1/\exp\left(-\mathbf{x}_i \boldsymbol{\beta}\right).`

The following code is an example of using the RWMH to estimate a logit model.

::
    
    #Import necessary libries
    import numpy as np
    from pymcmc.standard.glm import GLM
    from pymcmc.mcmc import MCMC, RWMH

    def posterior(store):
        """Function returns log posterior for logit model
        conditional on the current iteration of beta, which
        is a vector of regression coefficients.
        Note that store is a dictionary passed by the MCMC 
        engine that stores functions, classes, data, etc,
        used in the MCMC analysis.
        """

        #Note see declaration of class instance of logit in main
        #below.
        return store['logit'].log_posterior(store['beta'])

    #Main Routine
    np.random.seed(12345) #set seed
    kreg = 4 #Number of regressors
    N = 1000 #Number of observations
   
    #First we simulate data
    beta = np.random.randn(kreg)
    xmat = np.hstack([np.ones((N, 1)), np.random.randn(N, kreg-1)])
    yvec = np.zeros(N, dtype = 'i')
    xbeta = np.dot(xmat, beta)
    pvec = 1./np.exp(1. - xbeta)
    randu = np.random.rand(N)
    ind = pvec > randu
    yvec[ind] = 1

    #instantiate  GLM class. This is used to compute the posterior
    #obtain the Hessian and compute starting values for the algorithm.
    #A normal prior is used with a mean 0 and a precision which is a diagonal
    #matrix with 0.1 on the diagonal (variance of 10)

    logit = GLM(yvec, xmat, 'binomial',
               prior = ['normal', np.zeros(kreg), np.eye(kreg) * 0.1])

    #Note we store logit class instance in a dictionary called data
    #This dictionary is passed into MCMC engine and all functions called
    #from the engine have access to everything in this dictionary
    data = {'logit': logit}

    #Use Iterated weighted least squares to obtain inital values for algorithm.
    theta = logit.IWLS()

    #Use 2 times the negative inverse of the Hessian for the scale parameter
    #In the RWMH algorithm. 
    hess = logit.calc_hessian(theta)
    covariance = np.linalg.inv(-hess)
    cov_theta = 2.0 * covariance

    #Define block in MCMC sampling scheme to sample beta
    #Uses the random walk MH algorithm. Note the name 'beta' is used to
    #refer to iterates produced from the MCMC sampling scheme in various
    #functions used by the MCMC engine.
    rwmh = RWMH(posterior, covariance, theta, 'beta')

    #Intantiate MCMC class. The MCMC scheme will run for 20000 iteration of
    #which the first 5000 will be discarded. The dictionary data contains information
    #that is accessible to all functions called by the MCMC engine. The blocking scheme
    #consists of just one block (the RWMH algorithm), and a progress bar is to be printed.
    mcmc = MCMC(20000, 5000, data, [rwmh], runtime_output = True)
    mcmc.sampler() #Runs the MCMC sampler
    mcmc.output() #Produces standard output.


Output from the algorithm is as follows

::

    The time (seconds) for the MCMC sampler = 3.97
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]      -0.713      0.0824       -0.872      -0.552         18.5
         beta[1]       0.873      0.0912        0.701        1.05         25.1
         beta[2]       -1.02      0.0913         -1.2      -0.841         21.7
         beta[3]      -0.864      0.0885        -1.04        -0.7         23.3

    Acceptance rate  beta  =  0.48005

Adaptive RWMH
-------------
In adaptive Metropolis Hastings (MH) algorithms the proposal distributions evolves, using information in the previous iterations, to
attempt to improve the proposal distribution. There is a lot of recent research in this area, see for instance  
[HaarioSaksmanTamminen2001]_, [RobertsRosenthal2009]_ and [GarthwaiteFanSisson2010]_. PyMCMC currently supports two
alternative RWMH algorithms. In particular, the algorithm of [HaarioSaksmanTamminen2001]_ and the algorithm of
[GarthwaiteFanScisson]_. 

The algorithm of [HaarioSaksmanTamminen2001]_ tunes the covariance of the random walk candidate as a part of the 
estimation process. Specifically, let the covariance at iteration :math:`i` be :math:`\boldsymbol{\Sigma}_i` then

.. math:: \boldsymbol{\Sigma}_i=\begin{cases} \boldsymbol{\Sigma}_0 & i \le i_0 \\
    \tau\left(\mbox{cov}\left(\boldsymbol{\theta}_0,\boldsymbol{\theta}_1,\dots,\boldsymbol{\theta}_{i-1}\right)   + \varepsilon \mathbf{I}_k\right) & i>i_0, \end{cases}
where the covariance of :math:`\boldsymbol{\theta_0},\boldsymbol{\theta}_1,\dots,\boldsymbol{\theta}_{i-1},`
is computed through sequential updates at each iteration and :math:`\tau=2.4^2/k.`

Continuing with the same example, the algorithm of [HaarioSaksmanTamminen2001]_ in PyMCMC is demonstrated as follows:

::

    #just to demonstrate the initial covariance need not be inteligent
    #At least for simple problems
    covariance = np.eye(nreg)
    rwmh = RWMH(posterior, covariance, theta, 'beta', adaptive = 'HST')
    mcmc = MCMC(20000, 5000, data, [rwmh], runtime_output = True)
    mcmc.sampler()
    mcmc.output()

By default, we set for the case :math:`k=1,` :math:`i_0=100` and for the case :math:`k>1` we set 
:math:`i_0=100k^2.` This however can be changed through the optional argument 'HST_i0'. For example
suppose we wished to set :math:`i_0=1000` then we would instantiate the class :class:`RWMH` as

::

    rwmh = RWMH(posterior, covariance, theta, 'beta', adaptive = 'HST', HST_i0 = 1000)

The parameter :math:`\varepsilon` also has a default value that can be changed. For the scalar case
we set :math:`\varepsilon=0.0` as it is not required. For the case :math:`k>0` set to :math:`0.01.` This
can be changed using the option argument 'HST_epsilon'.

Output from the above example is as follows

:: 

    The time (seconds) for the MCMC sampler = 8.21
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]      -0.701      0.0793       -0.858      -0.553         18.7
         beta[1]       0.871      0.0857        0.712        1.05         21.6
         beta[2]       -1.03      0.0901         -1.2      -0.861         21.1
         beta[3]      -0.863       0.088        -1.04      -0.696         19.9

    Acceptance rate  beta  =  0.1334

Here we can see the output is much the same as the previous example, except in this
case we did not need to use any intelligent candidate. Really only the posterior
distribution is required.
An alterntaive to the algorithm of [HaarioSaksmanTamminen2001]_ that is available
as a part of PyMCMC can be used by setting *adaptive = True*. This scheme only requires
a scale parameter to initialise rather than an initial covariance. This is the prefered adaptive
scheme in PyMCMC. An example of the declearation of the class for the RWMH is as follows:

rwmh = RWMH(posterior, 1., theta, 'beta', adaptive = True)

The output from the scheme is given below:

::

    The time (seconds) for the MCMC sampler = 5.52
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]      -0.712      0.0816        -0.87      -0.548         15.2
         beta[1]       0.869      0.0899        0.713        1.06         17.1
         beta[2]       -1.03      0.0938        -1.22      -0.853         18.2
         beta[3]      -0.869       0.087        -1.03      -0.692         17.2

    Acceptance rate  beta  =  0.243

It can be seen that the mixing is similiar, but possibly slightly better, and the 
algorithm is also quicker. This algorithm scales the covariance and aims at a more
optimal acceptance rate.

Another alternative is the 
algorithm of [GarthwaiteFanScisson2010]_. In PyMCMC using the adaptive
algorithm of [GarthwaiteFanScisson2010]_ only requires an initial value for the scaling
constant, and the additional argument of *adaptive = 'GFS'*. An example of the declaration
of the class for the RWMH is as follows:

::

    rwmh = RWMH(posterior, 1., theta, 'beta', adaptive = 'GFS')

The corresponding output is given below.

::

    The time (seconds) for the MCMC sampler = 9.86
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]      -0.701      0.0835       -0.864      -0.537         15.7
         beta[1]       0.873      0.0889        0.708        1.05           17
         beta[2]       -1.03      0.0914        -1.21      -0.855         15.9
         beta[3]      -0.871      0.0883        -1.04      -0.705         16.7

    Acceptance rate  beta  =  0.2381
    
The mixing performance is similar, for this example, to that of our prefered
algorithm, however, we find our algorithm more robust and it is also faster.




References
----------

.. [GarthwaiteFanSisson2010] Garthwaite, P. Fan, Y, and Sisson, S., (2010), Adaptive Optimal Scaling of Metropolis Hastings \
 algorithms using the Robbins-Munro Process, 
.. [HaarioSaksmanTamminen2001] Haario, H., Saksman, E. and Tamminen, J., (2001), An adaptive Metropolis algorithm, Bernoulli, 7, 223--242.  
.. [RobertCassella1999] Robert, C. and Casella, G., (1999), Monte Carlo statistical methods, Springer-Verlag, New York.
.. [RobertsRosenthal2009] Roberts, G. and Rosenthal, J., (2007), Examples of adaptive MCMC, Journal of Computational \
    and Graphical Statistics, 18, 349--367.
    






