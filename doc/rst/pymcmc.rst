PyMCMC Algorithms
=================

PyMCMC includes many algorithms that can be used to produce tailored MCMC samplers.

.. toctree::
    :maxdepth: 2

    RandomWalkMetropolisHastings
    IndependentMetropolisHastings
    MetropolisHastings
    MALA
    SliceSlampler
    OBMC
    CFsampler
    MCMC
    HybridAlgorithms

Regression Tools
===============

.. toctree::
    :maxdepth: 2

    LinearModel
    CondRegressionSampler
    CondScaleSampler
    SSVS




