.. highlight:: rst

Introduction
========================

The most common approach currently used in the estimation of Bayesian
Models is Markov chain Monte Carlo (MCMC). PyMCMC is a
Python module that is designed to simplify the construction
of Markov chain Monte Carlo (MCMC) samplers, without sacrificing
flexibility or performance. Python has extensive scientific
libraries, is easily extensible, has a clean syntax and powerful
programming constructs, making it an ideal programming language to
build an MCMC library. PyMCMC contains objects
for the Gibbs sampler, Metropolis Hastings (MH), independent MH,
random walk MH, orientational bias Monte Carlo (OBMC) as well as the
slice sampler: see for example [RobertCassela1999]_ for details on
standard MCMC algorithms. The user can simply piece together the algorithms
required and can easily include their own modules, where necessary.
Along with the standard algorithms, PyMCMC includes a module for
Bayesian regression analysis. This module can be used for the direct
analysis of linear models, or as a part of an MCMC scheme, where the
conditional posterior has the form of a linear model.  It also
contains a class that can be used along with the Gibbs sampler for
Bayesian variable selection.

% PyMCMC is designed to be fully flexible, with respect to MCMC design.
The flexibility of PyMCMC is important in practice, as MCMC
algorithms usually need to be tailored to the problem of interest in
order to ensure good results.  Issues such as block size and
parameterisation can have a dramatic effect on the convergence of MCMC
sampling schemes. For instance, [LuiKongWong1994]_ show
theoretically that jointly sampling parameters in a Gibbs scheme
typically leads to a reduction in correlation in the associated Markov
chain in comparison to individually sampling parameters. This is
demonstrated in practical applications in [CarterKohn1994]_ and
[KimShephardChib1998]_. Reducing the correlation in the Markov
chain enables it to move more freely through the parameter space and
as such enables it to escape from local modes in the posterior
distribution. Parameterisation can also have a dramatic effect on the
convergence of MCMC samplers; see for example
[GelfandSahuCarlin1995]_, [RobersSahu1997]_,
[PittShepard1999]_, [RobertMengersen1999]_,
[FruwirthSchnatter2004]_ and [StricklandMartinForbes2008]_,
who show that the performance of the sampling schemes can be improved
dramatically with the use of efficient parameterisation.

PyMCMC aims to remove unnecessary repetitive coding and hence
reduce the chance of coding error, and importantly, greatly speed up
the construction of efficient MCMC samplers. This is achieved by
taking advantage of the flexibility of Python, which allows
for the implementation of very general code. Another feature of
Python, which is particularly important, is it is also
extremely easy to include modules from compiled languages such as
C and Fortran. This is important to many
practitioners who are forced, by the size and complexity of their
problems, to write their MCMC programs entirely in compiled languages,
such as C/C++ and Fortran in order to obtain the
necessary speed for feasible practical analysis. With
Python, the user can simply compile Fortran code
using a module called F2py [F2PY]_, or inline C
using Weave, which is a part of Scipy [NumpyScipy]_,
and use the subroutines directly from Python. F2py
can also be used to directly call C routines with the aid
of a Fortran signature file. This enables the use of
PyMCMC and Python as a rapid application development
environment, without compromising on performance by requiring only very
small segments of code written in a compiled language. It should be
mentioned that for most reasonable sized problems PyMCMC is
sufficiently fast for practical MCMC analysis without the need for
specialised modules.

.. [RobertCassela1999] C. P. Robert and G. Casella (1999) Monte Carlo 
Statistical Methods, Springer-Verlag, New York.



