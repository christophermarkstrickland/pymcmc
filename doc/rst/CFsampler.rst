Closed Form Sampling
====================

The class :class:`CFsampler` is used when the user wishes to sample a block of an MCMC scheme from a 
closed form solution. As a trivial example consider a linear regression model, where the 
:math:`(n\times 1)` vector of observations, :math:`\mathbf{y}` is generated from

.. math:: \mathbf{y}=\mathbf{X}\boldsymbol{\beta} + \boldsymbol{\varepsilon};\,\,\, 
    \boldsymbol{\varepsilon}\sim N(\mathbf{0}, \sigma^2\mathbf{I}),
    :label: LinearModel

then if one is to assume an independepent normal inverted gamma prior, where

.. math:: \boldsymbol{\beta}|\mathbf{y},\sigma\sim N(\underline{\boldsymbol{\beta}},\underline{\mathbf{V}}^{-1})\text{ and }
    \sigma|\mathbf{y},\boldsymbol{\beta}\sim IG\left(\frac{\nu}{2},\frac{S}{2}\right),

an appropriate MCMC scheme for sampling from the posterior distribution, 
:math:`p\left(\boldsymbol{\beta},\sigma|\mathbf{y}\right)` may be defined at iteration :math:`j` as follows:

1. Sample :math:`\boldsymbol{\beta}^{(j)}` from
   :math:`p\left(\boldsymbol{\beta}|\mathbf{y},\sigma^{(j-1)}\right).`

2. Sample :math:`\sigma^{(j)}` from :math:`p\left(\sigma|\mathbf{y},\boldsymbol{\beta}^{(j)}\right).`

Steps 1 and 2 of the algorithm above are straightforward to implement using PyMCMC. In particular, Step 1
can be computed in closed form using the class :class:`CondRegressionSampler` and Step 2 can be computed
in closed form using the class :class:`CondScaleSampler`; see Sections :doc:`CondRegressionSampler` and 
:doc:`CondScaleSampler`. An example is as follows:

::
    
    #import libraries
    import numpy as np
    from pymcmc.mcmc import MCMC, CFsampler
    from pymcmc.regtools import CondRegressionSampler, CondScaleSampler, LinearModel

    #set random number seed
    np.random.seed(12345)

    def simdata():
        "Function to simulate data"
        nobs = 1000 # number of observations
        nreg = 4 # number of regressors

        beta = np.random.randn(4)
        xmat = np.column_stack((np.ones(nobs), np.random.randn(nobs ,nreg - 1)))
        yvec = np.dot(xmat, beta) + 0.3 * np.random.randn(nobs)
        return yvec, xmat

    def simbeta(store):
        "Function to sample regression coefficients"

        #Note 'regsampler' and 'sigma' are defined in main function
        return store['regsampler'].sample(store['sigma'])

    def simsigma(store):
        "Function to sample scale parameter."

        #compute residuals, 'yvec', 'xmat' and 'beta' define in main function.
        res = store['yvec'] - np.dot(store['xmat'], store['beta'])
        #Note 'scale_sampler' defined in main function
        return store['scale_sampler'].sample(res)

    def main():
        "Main function for program"

        yvec, xmat = simdata() #Simulate data.

        #Add yvec and xmat to data dictionary so all functions called from MCMC
        #engine have access to them.
        data = {'yvec': yvec, 'xmat': xmat}

        #Add class CondScaleSampler to data, where nu = 10 and S = 0.01
        data['scale_sampler'] = CondScaleSampler(prior = ['inverted_gamma', 10, 0.01])

        #Add class CondRegressionSampler to data, with a prior mean of 0 and a prior
        #precision of 0.1 an identity matrix
        data['regsampler'] = CondRegressionSampler(yvec, xmat,
                 prior = ['normal', np.zeros(xmat.shape[1]), 0.1 * np.eye(xmat.shape[1])] )

        #Obtain initial values for parameters based on regression with Jeffreys prior
        breg = LinearModel(yvec, xmat)
        init_sigma, init_beta = breg.posterior_mean()

        #Define block for beta
        sample_beta = CFsampler(simbeta, init_beta, 'beta')

        #Define block for sigma
        sample_sigma = CFsampler(simsigma, init_sigma, 'sigma')

        #Define MCMC class, to run for 10000 iteration, where the first 1000
        #are discarded as burnin
        mcmc = MCMC(10000, 1000, data, [sample_beta, sample_sigma],
                    runtime_output = True)

        mcmc.sampler()
        mcmc.output()

    #Call main function
    main()

The output from the above MCMC sampling scheme is as follows:

::

    The time (seconds) for the MCMC sampler = 2.30
    Number of blocks in MCMC sampler =  2

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]      -0.204     0.00949       -0.224      -0.186         3.75
         beta[1]        0.49      0.0095        0.472       0.509         3.54
         beta[2]       -0.51     0.00945       -0.529      -0.491         3.56
         beta[3]      -0.553     0.00932       -0.571      -0.534         3.75
           sigma       0.297     0.00666        0.284        0.31         3.53

    Acceptance rate  beta  =  1.0
    Acceptance rate  sigma  =  1.0

