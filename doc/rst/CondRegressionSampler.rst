Conditional Regression Sampler
==============================

The class :class:`CondRegressionSampler` is useful when a block in an MCMC scheme has
a full conditional posterior distribution that has a linear regression type structure.
In our experience this is quite common. As an example, consider a regression model
with first order heteroskedastic errors; see [JudgeHillGriffithsLutekepohlLee1988]_ for details. That is, where the :math:`i^{th}` observation
:math:`y_i` is generated from

.. math:: y_i=\mathbf{x}_i^T\boldsymbol{\beta}+e_i,\,\,\, e_i\sim N(0,\sigma^2_i),
    :label: hetero_reg
where :math:`\sigma^2_i=\exp{\mathbf{z}_i^T\boldsymbol{\alpha}}`. This model
may be expressed as

.. math:: \tilde{\mathbf{y}}=\tilde{\mathbf{X}}\boldsymbol{\beta}+\tilde{\boldsymbol{\varepsilon}},
    :label: WLS
where :math:`\tilde{\mathbf{y}}=\boldsymbol{\Omega}^\frac{1}{2}\mathbf{y}`,
:math:`\tilde{\mathbf{X}}=\boldsymbol{\Omega}^\frac{1}{2}\mathbf{X}` and
:math:`\tilde{\boldsymbol{\varepsilon}}=\boldsymbol{\Omega}^\frac{1}{2}\boldsymbol{\varepsilon}`, and
:math:`\boldsymbol{\Omega}^{-\frac{1}{2}}=\mbox{diag}\left(\mathbf{z}_1^T\boldsymbol{\alpha}_1,\mathbf{z}_2^T
\boldsymbol{\alpha}_2,\dots,\mathbf{z}_n^T\boldsymbol{\alpha}\right).` Note that 
:math:`\boldsymbol{\varepsilon}^*\sim N\left(\mathbf{0}, \mathbf{I}\right).`

A possible MCMC scheme, at iteration :math:`j` is as follows:

1. Sample :math:`\boldsymbol{\beta}^{(j)}` from :math:`p\left(\boldsymbol{\beta}|\mathbf{y},\mathbf{X},\mathbf{Z},\boldsymbol{\alpha}^{(j-1)}\right)`.
2. Sample :math:`\boldsymbol{\alpha}^{(j)}` from :math:`p\left(\boldsymbol{\alpha}|\mathbf{y},\mathbf{X},\mathbf{Z},\boldsymbol{\beta}^{(j)}\right)`.

In the sampling scheme above the posterior distribution for :math:`\boldsymbol{\beta},` is simply equivalent
to a Bayesian regression, on the model :eq:`WLS`, with a known covariance structure, and thus can be drawn in
a straightforward manner using the class :class:`CondRegressionSampler`. Numerous sampling schemes could be
defined to sample :math:`\boldsymbol{\alpha}` from its posterior distribution defined by Step 2. One possiblity is 
to use an adaptive version of the Metropolis adjusted Langevin algorithm (MALA); See Section :doc:`MALA` for further details.
Assuming a flat prior for :math:`\boldsymbol{\alpha}` the posterior 
for the heteroskedastic regression model in :eq:`hetero_reg` is given by

.. math:: p\left(\boldsymbol{\alpha}|\mathbf{y},\mathbf{X},\mathbf{Z},\boldsymbol{\beta}\right) \propto
    -\frac{1}{2}\sum_{i=1}^n\left(\mathbf{z}_t^T\boldsymbol{\alpha}+\exp(-\mathbf{z}_t^T\boldsymbol{\alpha})
    (y_i-\mathbf{x}_t^T\boldsymbol{\beta})^2\right)
    :label: posterior

and the gradient vector is given by

.. math:: \frac{\partial \log p\left(\boldsymbol{\alpha}|\mathbf{y},\mathbf{X},\mathbf{Z},\boldsymbol{\beta}\right)}
    {\partial \boldsymbol{\alpha}} = -\frac{1}{2} \sum_{i=1}^n \mathbf{z}_t \left(1-\exp(\mathbf{z}_t^T
    \boldsymbol{\alpha})(y_i-\mathbf{x}_i^T\boldsymbol{\beta})^2\right).
    :label: gradient

If we define functions for the log posterior corresponding to :eq:`posterior` and for the gradient vector
:eq:`gradient` then it is straightforward to implement the MALA algorithm.
As an example, an MCMC scheme is implemented below, which resembles an example of a 
heteroskedastic regression in [JudgeHillGriffithsLutekepohlLee1988]_ (pages
540-541). 

::

    #import libraries
    import numpy as np
    from pymcmc.regtools import LinearModel, CondRegressionSampler
    from pymcmc.mcmc import MCMC, CFsampler, MALA

    def simdata():
        #Simulate Data (following Judge et al (2008, pp 540-541).
        nobs = 1000 #number of observations
        nreg = 3 #number of regressors

        beta = np.array((0.91, 1.603, 0.951)) #Values for regression coefficients
        alpha = np.array((-1.195, 0.217))

        xmat = np.column_stack((np.ones(nobs), np.random.randn(nobs, nreg - 1)))
        zmat = xmat[:, 1:] #Use regressors 1 and 2

        sigma = np.exp(np.dot(zmat, alpha) / 2)
        yvec = np.dot(xmat, beta) + sigma * np.random.randn(nobs)

        return yvec, xmat, zmat

    def sample_beta(store):
        """Function samples beta from its full conditional
        posterior distribution, using WLS with known covariance."""

        #computes weights
        weight = np.exp(-np.dot(store['zmat'], store['alpha']) / 2.)

        #updates weighted observations and regressors
        store['reg_sampler'].update_yvec(store['yvec'] * weight)
        store['reg_sampler'].update_xmat(store['xmat'] * weight[:, np.newaxis])

        #Samples from posterior distibution for beta. Note the scale parameter = 1
        return store['reg_sampler'].sample(1.)

    def log_posterior_alpha(store):
        """Function computes log posterior for alpha."""

        #Compute zmat * alpha
        zalpha = np.dot(store['zmat'], store['alpha'])

        #compute y - xmat * beta
        res = store['yvec'] - np.dot(store['xmat'], store['beta'])

        #evaluate log posterior
        return -0.5 * (zalpha + np.exp(-zalpha) * res ** 2).sum()

    def gradient_vector(store):
        "Function evalutes gradient vector for alpha."

        #Compute zmat * alpha
        zalpha = np.dot(store['zmat'], store['alpha'])
        

        #compute y - xmat * beta
        res = store['yvec'] - np.dot(store['xmat'], store['beta'])

        #computes gradient vector
        return -0.5 * (store['zmat'] * (1. - np.exp(-zalpha) * res ** 2)[:, np.newaxis]).sum(0)

    def main():
        "Main program starts here."
        np.random.seed(12345) # Seed random number generator

        yvec, xmat, zmat = simdata() #Simulates data

        #Define data dictionary
        data = {'yvec': yvec, 'xmat': xmat, 'zmat': zmat}

        #Setup class instance to sample beta (Using Jeffreys prior)
        data['reg_sampler'] = CondRegressionSampler(yvec, xmat)

        #Use linear model to obtain initial values
        breg = LinearModel(yvec, xmat)
        dsigma, init_beta = breg.posterior_mean()
        simbeta = CFsampler(sample_beta, init_beta, 'beta')

        #Setup block for alpha
        simalpha = MALA(log_posterior_alpha, 1., gradient_vector, np.zeros(2),
            'alpha', adaptive = 'MR')

        #Setup class instance for MCMC; 20000 iterations, where the first
        #5000 are discarded as burnin
        mcmc = MCMC(20000, 5000, data, [simbeta, simalpha],
                    runtime_output = True)
        mcmc.sampler()
        mcmc.output()

    #call main function
    main()

The output from the example above is as follows:

::

    The time (seconds) for the MCMC sampler = 14.55
    Number of blocks in MCMC sampler =  2

                        mean          sd         2.5%       97.5%      IFactor
        alpha[0]       -1.22      0.0352        -1.29       -1.15         4.73
        alpha[1]       0.217      0.0354        0.149       0.288         4.86
         beta[0]       0.933      0.0343        0.869           1         3.57
         beta[1]        1.59      0.0206         1.55        1.64         3.75
         beta[2]       0.923      0.0204        0.882       0.962         3.56

    Acceptance rate  alpha  =  0.5725
    Acceptance rate  beta  =  1.0

It is clear from the inneficiency factors that the mixing of the algorithm is very good.


    


References
----------

.. [JudgeHillGriffithsLutekepohlLee1988] Judge, G., Hill, C., Griffiths, W., Lutkepohl, H.
and Lee, T. (1988), Introduction to theory and practice of Econometrics, Wiley and Sons.
