Metropolis Adjusted Langevin Algorithm
======================================

The Metropolis adjusted Langevin algorithm (MALA) uses the discretisation of the Langevin diffusion
to form a candidate in a Markov chain Monte Carlo (MCMC) sampling scheme.  Suppose that the posterior
distribution of interest, for the :math:`(k\times 1)` vector of unknown parameters 
:math:`\boldsymbol{\theta}`, is :math:`\pi\left(\boldsymbol{\theta}\right)`, then the candidate for the MALA
algorithm that is used in a Metropolis Hastings (MH) kernel generates the candidate,
:math:`\boldsymbol{\theta}^*` at iteration :math:`i+1` from

.. math:: \boldsymbol{\theta}^*=\boldsymbol{\theta}^{(i)}+\frac{\sigma^2}{2}\nabla\mbox{log}\pi\left(\boldsymbol{\theta}^{(i)}\right)
    + \sigma\varepsilon_i,
where :math:`\varepsilon_i` is a standard normal random number and, the drift term,
:math:`\nabla\mbox{log}\pi(\boldsymbol{\theta}^{(i)})`
is the gradient vector, i.e. :math:`\frac{\partial\pi(\boldsymbol{\theta})}{\partial\boldsymbol{\theta}},`
evaluated at :math:`\boldsymbol{\theta}^{(i)}`.

The class :class:`MALA` is is used to implement the MALA algorithm in PyMCMC. To first demonstrate this class
we use the same example as we did for the Section :doc:`../RandomWalkMetropolisHastings`. In particular, we again consider the Bernoulli logit regression model.

::

    #Import necessary libraries
    import os; import pymcmc
    import numpy as np
    from pymcmc.mcmc import MCMC, MALA
    from pymcmc.standard.glm import GLM

    def posterior(store):
        """Function returns log posterior for logit model
        conditional on the current iteration of beta, which
        is a vector of regression coefficients.
        Note that store is a dictionary passed by the MCMC
        engine that stores functions, classes, data, etc,
        used in the MCMC analysis.
        """

        #Note see declaration of class instance of logit in main
        #below.
        return store['logit'].log_posterior(store['beta'])

    def gradient(store):
        "Function returns the gradient vector for the posterior."

        return store['logit'].calc_score(store['beta'])


    #Main program
    np.random.seed(12345)       # seed for the random number generator
    kreg = 4 #Number of regressors
    N = 1000 #Number of observations

    #First we simulate data
    beta = np.random.randn(kreg)
    xmat = np.hstack([np.ones((N, 1)), np.random.randn(N, kreg-1)])
    yvec = np.zeros(N, dtype = 'i')
    xbeta = np.dot(xmat, beta)
    pvec = 1./np.exp(1. - xbeta)
    randu = np.random.rand(N)
    ind = pvec > randu
    yvec[ind] = 1

    #Create class instance for GLM class
    #Here we are assuming a flat prior. (i.e. we have not supplied
    #Additional prior information so we are just using the default.
    logit = GLM(yvec, xmat, 'binomial')

    #Create dictionary used by MCMC class engine. Note all functions calls
    #by the MCMC engine have access to this function.
    data = {'logit': logit}

    #Use iterative weighted least squares to get initial values
    init_beta = logit.IWLS()

    #Instantiate class for MALA. Note here we manually 
    #tune the scale parameter to 0.112.
    mala = MALA(posterior, 0.112, gradient, init_beta, 'beta')

    #Setup class for MCMC, 20000 iterations where the first 5000
    #are discarded as burnin
    mcmc = MCMC(20000, 5000, data, [mala], runtime_output = True) 

    #Run sampler and produce output
    mcmc.sampler()
    mcmc.output()


Output from the above example is as follows:

::

    The time (seconds) for the MCMC sampler = 17.73
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]      -0.709      0.0835       -0.876      -0.547         4.66
         beta[1]       0.875      0.0893        0.701        1.05         5.03
         beta[2]       -1.03      0.0916        -1.21      -0.853         5.26
         beta[3]      -0.869      0.0893        -1.04      -0.693         4.96

    Acceptance rate  beta  =  0.5659

From the output above we can see that the mixing for the MALA algorithm (based on the inefficiency factors)
is very good. It is clearly better than the RWMW algorithm we used on the same simulated data set in Section
:doc:`../RandomWalkMetropolisHastings`.

Issues with the standard algorithm for MALA
-------------------------------------------

It is important to note however, that the MALA algorithm does not always perform well
and it is far more sensitive to tuning than the RWMH algorithm. Take for example the following Poisson regression model,
with a log linear link function. That is where the likehood function for the :math:`(n\times 1)` vector of observations, 
:math:`\mathbf{y}` is given by

.. math:: p\left(\mathbf{y}|\boldsymbol{\theta}\right)=\prod_{i=1}^n\frac{\exp\{-\boldsymbol{\lambda}_i\}\boldsymbol{\lambda}_i^{y_i}}{y_i !,}
with :math:`\mbox{log}\lambda_i=\mathbf{x}_i^T\boldsymbol{\beta},`  where :math:`\mathbf{x}_i` is a 
:math:`(k\times 1)` vector of regressors and :math:`\boldsymbol{\beta}` is a :math:`(k\times 1)` vector of regression
coefficients. Consider the following code the MALA algorithm is used to estimate the log linear Poisson regression model.

::

    #Import necessary libraries
    import os; import pymcmc
    import numpy as np
    from pymcmc.mcmc import MCMC, MALA
    from pymcmc.standard.glm import GLM

    def posterior(store):
        """Function returns log posterior for loglinear model
        conditional on the current iteration of beta, which
        is a vector of regression coefficients.
        """

        #Note see declaration of class instance of loglinear in main
        #below.
        return store['loglinear'].log_posterior(store['beta'])

    def gradient(store):
        "Function returns the gradient vector for the posterior."

        return store['loglinear'].calc_score(store['beta'])

    #Main program
    np.random.seed(12345)       # seed for the random number generator

    #Loads data from file
    #Get the path for the data. If this was installed using setup.py
    #it will be in the data directory of the module
    datadir = os.path.join(os.path.dirname(pymcmc.__file__), 'data')
    data = np.loadtxt(os.path.join(datadir,'count.txt'), skiprows = 1)
    yvec = data[:, 0]
    xmat = data[:, 1:data.shape[1]]
    #Add constant to regressors
    xmat = np.column_stack([np.ones(data.shape[0]), xmat])

    #Create class instance for GLM class
    #Here we are assuming a flat prior. (i.e. we have not supplied
    #Additional prior information so we are just using the default.
    loglinear = GLM(yvec, xmat, 'poisson')

    #Create dictionary used by MCMC class engine. Note all functions calls
    #by the MCMC engine have access to this function.
    data = {'loglinear': loglinear}

    #Use iterative weighted least squares to get initial values
    init_beta = loglinear.IWLS()

    #Instantiate class for MALA
    mala = MALA(posterior, 5.23985E-9, gradient, init_beta, 'beta')

    #Setup class for MCMC, 20000 iterations where the first 5000
    #are discarded as burnin
    mcmc = MCMC(20000, 5000, data, [mala], runtime_output = True) 

    #Run sampler and produce output
    mcmc.sampler()
    mcmc.output()

Here the scale parameter is :math:`5.23985\times 10^{-9}` this gives us 100 percent acceptance rate, yet
if we change the scale parameter to :math:`5.23986\times 10^{-9}` this gives 0 percent acceptance rate. This
is obviously not really acceptable.

Full Manifold Metropolis Adjusted Langevin Algorithm
----------------------------------------------------

One possible improvement over the standard MALA algorithm is proposed in [GirolamiCalderhead2011]_. In particular,
the authors propose generating the candidate for the MH algorithm, at iteration :math:`i`, for :math:`\boldsymbol{\theta}^*`, from 

.. math:: \boldsymbol{\theta}^*=\boldsymbol{\theta}^{i-1} + \frac{\sigma^2}{2} G\left(\boldsymbol{\theta}^{i-1}\right)^{-1}\nabla\mbox{log}\pi\left(\boldsymbol{\theta}^{(i)}\right) + \sigma G\left(\boldsymbol{\theta}^{i-1}\right)^\frac{1}{2}\varepsilon_i,
    :label: MMALA
where :math:`\sigma` is a tuning parameter and 

.. math:: G\left(\boldsymbol{\theta}\right)=-E\left[\frac{\partial^2}{\partial\boldsymbol{\theta}^2}\mbox{log} p\left(\mathbf{y}|\boldsymbol{\theta}\right)\right]-\frac{\partial^2}{\partial\boldsymbol{\theta}^2}\mbox{log} g(\boldsymbol{\theta}),
where :math:`p\left(\mathbf{y}|\boldsymbol{\theta}\right)` is the likelihood function and
:math:`g\left(\boldsymbol{\theta}\right)` is the prior distribution. The authors refer to the modified MALA algorithm, which
uses the candidate in :eq:`MMALA` as the full manifold Metropolis adjusted Langevin algorithm (MMALA).

It is straightforward to implement the MMALA algorithm using PyMCMC. We simply need a function that either evaluates
:math:`G\left(\boldsymbol{\theta}\right)`.For example in the above problem we could approximate 
:math:`G\left(\boldsymbol{\theta}\right)` using the observed Fisher information matrix, noting that we can ignore the Hessian
for the prior as we are assuming a flat prior for the log-linear example. In this case the class instance for the MMALA 
algorithm is defined as 

::

    #Instantiate class for MMALA
    mala = MALA(posterior, scale, loglinear.calc_score, init_beta, 'beta',
            hessian = loglinear.calc_hessian)

Here *scale* refers to the tuning parameter, :math:`\sigma`. Note the only difference between the MALA algorithm
and the MMALA algorithms is we supply an additional 
argument *hessian*, which is used to take the function that evaluates :math:`-G\left(\boldsymbol{\theta}\right)`. 
For the problem above however, the MMALA algorithm has the same problem for tuning as the MMALA algorithm.


Adaptive MALA (Solving the tuning problem)
------------------------------------------

While the MALA algorithm can perform well they can be very difficult to tune. As a solution it possible
to adapt both the scaling constant as well as :math:`G\left(\boldsymbol{\theta}\right)^{-1}.` One 
particular solution is proposed in [MarshallRoberts2012]_. In PyMCMC, there is the option to use
a modified version of their algorithm. The following code demonstrates how to use an adaptive 
version of their algorithm for the scaling parameter in the MMALA algorithm.

::

    mala = MALA(posterior, 1., loglinear.calc_score,
                init_beta, 'beta',
                hessian = loglinear.calc_hessian, adaptive = 'MR')

    #Setup class for MCMC, 20000 iterations where the first 5000
    #are discarded as burnin
    mcmc = MCMC(20000, 5000, data, [mala], runtime_output = True)

    #Run sampler and produce output
    mcmc.sampler()
    mcmc.output()

The output from their algorithm is as follows.

::

    The time (seconds) for the MCMC sampler = 9.14
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]         2.1      0.0247         2.05        2.15         4.09
         beta[1]       0.667      0.0176        0.633       0.701         4.23

    Acceptance rate  beta  =  0.57765



It is clear from the output that the adaptive scheme works extremely well for the above example. 
The inefficieny factors demonstate that the mixing is very good. Note that the only difference in the 
algorithm is the extra argument *adaptive*. In the above example we also supplied the Hessian for the
algorithm. If this is not supplied then an adaptive estimate is used. In this case the MALA algorithm
is declared as follows:

:: 
     
    mala = MALA(posterior, 1., loglinear.calc_score,
                init_beta, 'beta',
                adaptive = 'MR')

Replacing the definition of *mala* with this in the above code produces the following
output.

::

    The time (seconds) for the MCMC sampler = 8.15
    Number of blocks in MCMC sampler =  1

                        mean          sd         2.5%       97.5%      IFactor
         beta[0]         2.1      0.0188         2.06        2.13         8.61
         beta[1]       0.668      0.0142        0.639       0.694         6.55

    Acceptance rate  beta  =  0.57455 

Here we can again see the mixing is very good, although the innefficiency factors are slightly higher
than when the Hessian is supplied. 

References
----------

.. [GirolamiCalderhead2011] Girolami, M. and Calderhead, B., (2011), Riemann Manifold Langevin and Hamiltonian Monte Carlo Methods, Journal of the Royal Statistical Society, Series B, 73, 123--214.

.. [MarshallRoberts2012] Marshall, T. and Roberts, G., (2012), An adaptive approach to Langevin MCMC, Statistics and
    Computing, 22, 1041--1057.
