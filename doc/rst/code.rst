Code
====

.. automodule:: mcmc
   :members:

.. automodule:: regtools
   :members:

.. automodule:: standard.glm
   :members:
