.. pymcmc documentation master file, created by
   sphinx-quickstart on Thu Jul  7 11:53:44 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pymcmc's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2
   
   introduction
   code
   standard
   pymcmc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

