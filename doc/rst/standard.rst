Standard Models
====================

The standard models module is a set of pre-optimised algorithms for some
regularly used models.

.. toctree::
    :maxdepth: 2

    linear_regression
    glm
    mixture_models



