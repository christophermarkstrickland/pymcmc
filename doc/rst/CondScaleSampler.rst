CondScaleSampler
================

The class :class:`CondScaleSampler` is used for sampling the scale or covariance parameter from a 
conditionally (multivariate) normal posterior distribution. This can be particularly convient in
MCMC sampling schemes. Four alternative prior assumptions are currently incorporated in PyMCMC.
In particular, three alternative priors for univariate data and one specific prior for multivariate
data. 

Let :math:`\mathbf{r}` be a residual vector from a normally distributed posterior and
:math:`\mathbf{r}|\boldsymbol{\theta}\sim N(\mathbf{0},\sigma^2\mathbf{I})`, where
:math:`\boldsymbol{\theta}` are a set of parameters that are conditionally known, when sampling
:math:`\sigma`. The default prior assumption is Jeffreys prior, that is

.. math:: \sigma|\boldsymbol{\theta} \propto \frac{1}{\sigma}.
    :label: Jeffreys

The alternative prior assumption is that  :math:`\sigma` follows an inverted gamma distribution
where,

.. math:: \sigma|\boldsymbol{\theta}\sim IG\left(\frac{\nu}{2},\frac{S}{2}\right).
    :label: InvertedGamma

This prior is specified through the optional argument *prior* where the specification is passed using
the form ['inverted_gamma', :math:`\nu,S`].

Alternatively one may prefer to specify the prior in terms on the precision in which case we can
assume that :math:`\sigma^{-2}` follows an gamma distribution, such that

.. math::  \sigma^{-2}|\boldsymbol{\theta}\sim G\left(\frac{\nu}{2},\frac{S}{2}\right).
    :label: Gamma

In this case the argument *prior* is specified as ['gamma', :math:`\nu`, S].

For multivariate models, let :math:`\mathbf{r}|\boldsymbol{\theta}\sim N(\mathbf{0},\boldsymbol{\Sigma}^{-1})`,
then a possible prior specification is to assume :math:`\boldsymbol{\Sigma}` follows a Wishart distribution, such
that

.. math:: \boldsymbol{\Sigma}|\boldsymbol{\theta}\sim W\left(\nu,\mathbf{S}^{-1}, \right).
For the Wishart case the argument *prior* is specified as ['wishart', :math:`\nu, \mathbf{S}`].

Examples
--------

Suppose the vector of observations, :math:`\mathbf{y}_t`, that are generated from the state space model

.. math:: \mathbf{y}_t=\mathbf{Z}_t\mathbf{x}_t+
    \mathbf{R}_t\boldsymbol{\varepsilon}_t;
    \,\,\, \boldsymbol{\varepsilon}_t\sim N\left(\mathbf{0}, \mathbf{H}_t\right),
    :label: Observation_SSM    

where the state vector :math:`\mathbf{x}_t` is generated from  

.. math:: \mathbf{x}_{t+1}=
    \mathbf{T}_t\mathbf{x}_t+\mathbf{G}_t \boldsymbol{\eta}_t;\,\,\, \boldsymbol{\eta}_t\sim N\left(\mathbf{0}, 
    \mathbf{Q}_t\right).
    :label: State_SSM

Suppose :math:`Z_t=1`, :math:`H_t=\sigma^2_{\varepsilon}`, :math:`T_t=1`, :math:`G_t=1` and
:math:`Q_t=\sigma^2_{\eta},` then :eq:`Observation_SSM` and :eq:`State_SSM` refer to the local level model.
In the following example PyMCMC is used to estimate the local level model.

.. plot:: pyplots/local_level.py
    :include-source:
    
        

