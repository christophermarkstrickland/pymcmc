#Import necessary libraries
import os; import pymcmc
import numpy as np
from pymcmc.mcmc import MCMC, MALA
from pymcmc.standard.glm import GLM

def posterior(store):
    """Function returns log posterior for loglinear model
    conditional on the current iteration of beta, which
    is a vector of regression coefficients.
    Note that store is a dictionary passed by the MCMC
    engine that stores functions, classes, data, etc,
    used in the MCMC analysis.
    """

    #Note see declaration of class instance of loglinear in main
    #below.
    lnpr = store['loglinear'].log_posterior(store['beta'])
    return lnpr

def gradient(store):
    "Function returns the gradient vector for the posterior."

    return store['loglinear'].calc_score(store['beta'])
    




#Main program
np.random.seed(12345)       # seed for the random number generator

#Loads data from file
#Get the path for the data. If this was installed using setup.py
#it will be in the data directory of the module
datadir = os.path.join(os.path.dirname(pymcmc.__file__), 'data')
data = np.loadtxt(os.path.join(datadir,'count.txt'), skiprows = 1)
yvec = data[:, 0]
xmat = data[:, 1:data.shape[1]]
xmat = (xmat - xmat.mean(0)) / xmat.std(0)
#Add constant to regressors
xmat = np.column_stack([np.ones(data.shape[0]), xmat])

#Create class instance for GLM class
#Here we are assuming a flat prior. (i.e. we have not supplied
#Additional prior information so we are just using the default.
loglinear = GLM(yvec, xmat, 'poisson')
#loglinear.output()

#Create dictionary used by MCMC class engine. Note all functions calls
#by the MCMC engine have access to this function.
data = {'loglinear': loglinear}

#Use iterative weighted least squares to get initial values
init_beta = loglinear.IWLS()

#Instantiate class for MALA
#mala = MALA(posterior, 5.312985378494039495E-9, loglinear.calc_score,
#            init_beta, 'beta',
#            adaptive = 'MR')
scale = 0.0000023287135795

mala = MALA(posterior, 1., gradient,
            init_beta, 'beta', adaptive = 'MR')#,
            #hessian = loglinear.calc_hessian, adaptive = 'MR')

#Setup class for MCMC, 20000 iterations where the first 5000
#are discarded as burnin
mcmc = MCMC(20000, 5000, data, [mala], runtime_output = True)

#Run sampler and produce output
mcmc.sampler()
mcmc.output()

#from pymcmc.mcmc import RWMH
#rwmh = RWMH(posterior, 1., init_beta, 'beta', adaptive = 'GFS')
#        
#mcmc = MCMC(20000, 5000, data, [rwmh], runtime_output = True)

##Run sampler and produce output
#mcmc.sampler()
#mcmc.output()


