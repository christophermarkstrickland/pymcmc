# bayesian MCMC estimation of binary choice models: Logit model
import numpy as np

#from glm_probit import Probit
from pymcmc.standard.glm import GLM

def simdata(N, kreg):
    """function simulates binomial data for probit model"""
    beta = np.random.randn(kreg)
    print "simulated beta = ", beta
    xmat = np.hstack([np.ones((N, 1)), np.random.randn(N, kreg-1)])
    sigma = 1.0
    yvec = np.zeros(N, dtype = 'i')
    ystar = np.dot(xmat, beta) + sigma * np.random.randn(N)
    yvec[ystar >= 0] = 1
    yvec[ystar < 0] = 0

    return yvec, xmat, beta




#Main program starts here
def main():
    np.random.seed(12345)
    nobs = 1000
    kreg = 8
    yvec, xmat, simbeta = simdata(nobs, kreg)

    probit = GLM(yvec, xmat, 'binomial-probit', runtime_output = True)
    probit.output(parameters = 'beta')
    probit.plot('beta')
    print simbeta

main()
