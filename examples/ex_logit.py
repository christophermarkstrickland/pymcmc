# bayesian MCMC estimation of binary choice models: Logit model
import numpy as np

from pymcmc.standard.glm import GLM
import cProfile
import pstats


def logit_link(x):
    return 1. / (1 + np.exp(-x))


def simdata(N, kreg):
    """function simulates binomial data for logit model"""
    beta = np.random.randn(kreg)
    print "simulated beta = ", beta
    xmat = np.hstack([np.ones((N, 1)), np.random.randn(N, kreg - 1)])
    yvec = np.zeros(N, dtype='i')
    xbeta = np.dot(xmat, beta)
    pvec = logit_link(xbeta)
    randu = np.random.rand(N)
    ind = pvec > randu
    yvec[ind] = 1
    return yvec, xmat, beta


#Main program starts here
np.random.seed(12345)
nobs = 1000
kreg = 4


def main():
    yvec, xmat, simbeta = simdata(nobs, kreg)

    #Test default algorithm (5)
    print "Test algorithm 5"
    logit = GLM(yvec, xmat, family='binomial', runtime_output=True)
    logit.output()
    #logit.plot('beta')
    #print "Test algorithm 1 (RWMH)"
    #logit = GLM(yvec, xmat, family='binomial', algorithm=1,
    #            runtime_output=True)
    #logit.output()

    #print "Test algorithm 2 (MALA)"
    #logit = GLM(yvec, xmat, family='binomial', algorithm=1,
    #            runtime_output=True)
    #logit.output()

    #print "Test algorithm 3 (MMALA)"
    #logit = GLM(yvec, xmat, family='binomial', algorithm=3,
    #            runtime_output=True)
    #logit.output()

    #print "Test algorithm 4 (ARWMH)"
    #logit = GLM(yvec, xmat, family='binomial', algorithm=4,
    #            runtime_output=True)
    #logit.output()
#main()

cProfile.run('main()', 'mainprof')
p = pstats.Stats('mainprof')

p.sort_stats('time')
p.print_stats(10)
