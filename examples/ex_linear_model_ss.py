# example code for variable selection in regression
# using LinearModel class

import os
import numpy as np
from pymcmc.regtools import LinearModel
import pymcmc

def main():
    """ get the path for the data. If this was installed using setup.py
    it will be in the data directory of the module"""
    datadir = os.path.join(os.path.dirname(pymcmc.__file__),'data')

    # main program
    np.random.seed(12346)

    # loads data
    data = np.loadtxt(os.path.join(datadir,'yld2.txt'))
    yvec = data[:, 0]
    xmat = data[:, 1:20]
    xmat = np.column_stack([np.ones(xmat.shape[0]), xmat])
    nreg = xmat.shape[1]

    #Linear model with normal inverted gamma prior
    #D = np.column_stack([np.ones(nreg) * 6., np.ones(nreg) * 0.06])
    #R = np.eye(nreg)
    #nu = 10
    #S = 0.01
    #nig_prior_ss = ['normal_inverted_gamma_SS', nu, S, R, D]
    #breg = LinearModel(yvec, xmat, prior = nig_prior_ss)
    #breg.output()

    #Linear model with normal inverted gamma spike slab prior
    nu = 10
    S = 0.01
    bubar = np.zeros(nreg)
    vubar = np.eye(nreg) * 0.1

    nig_prior_ss_spike_slab = ['normal_inverted_gamma_SS_spike_slab', nu, S, bubar, vubar]
    breg = LinearModel(yvec, xmat, prior = nig_prior_ss_spike_slab)
    breg.output()

    ##Linear Model with g-prior
    #g_prior = ['g_prior_SS', 0.0, 100.]
    #breg = LinearModel(yvec, xmat, prior = g_prior)
    #breg.output()

main()
