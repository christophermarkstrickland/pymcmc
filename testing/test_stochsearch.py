#Python code used in testing functions in stochsearch

import unittest
from pymcmc import stochsearch
import numpy as np

class TestStochsearch(unittest.TestCase):
    """Class used for performing unit tests on stochsearch."""

    def setUp(self):
        #set seed
        np.random.seed(12345)

        self.nobs = 100 #number of observations
        self.kreg = 5 #number of regressors

        #define regression coefficients
        self.beta = np.ones(self.kreg)
        self.beta[2:4] = 0.0

        #simulate regressors
        self.xmat = np.asfortranarray(np.random.randn(self.nobs, self.kreg))
        sigma = 0.3

        #simulate data for testing
        self.yvec = np.dot(self.xmat, self.beta) + sigma \
                * np.random.randn(self.nobs)

        #compute quantities used in calculations
        self.ypy = np.dot(self.yvec, self.yvec)
        self.xpx = np.asfortranarray(np.dot(self.xmat.T, self.xmat))
        self.xpy = np.dot(self.xmat.T, self.yvec)

    def test_SS_gprior_spsl(self):
        """Tests gprior to see if it gets the same output as the
        normal inverted gamma spike slab prior (implemented manually as gprior with the same."""

        
        #allocating memory for gprior
        xgxg = np.zeros((self.kreg, self.kreg), order = 'F')
        xgy = np.zeros(self.kreg)
        info = np.array(0, dtype = 'i')
        info2 = np.array(0, dtype = 'i')
        gam = np.zeros(self.kreg, dtype = 'i')
        gam[0] = 1
        work = np.zeros((self.kreg, 6), order = 'F')
        work2 = np.zeros((self.kreg, self.kreg), order = 'F')


        #g parameter in gprior
        g = 100.

        #reset seed and draw random numbers
        np.random.seed(12345)
        ru = np.random.rand(self.kreg)
        

        #run stochastic search routine for g-prior
        stochsearch.ssreg(self.ypy, g, self.xpx, self.xpy, xgxg,
                          xgy, gam, ru, work, work2, info, info2, 
                         self.nobs)

        g_gamma = gam.copy()

        #compute ML g_prior
        tgamma = np.zeros(self.kreg, dtype = 'i')
        tgamma[:2] = 1
        qgam = tgamma.sum()
        ind = tgamma == 1
        xg = self.xmat[:,ind]
        t_xgxg = np.dot(xg.T, xg)
        t_xgy = np.dot(xg.T, self.yvec)
        ML_gprior =  self.compute_ML_gprior(qgam,
                       g, t_xgxg, t_xgy)

        #setup normal inverted gamma as g_prior
        vubar = self.xpx / g
        vobar = self.xpx + vubar
        nu = 0
        su = 0.
        nuo = self.nobs + nu

        #Allocate workspace for allgorithms
        gam = np.zeros(self.kreg, dtype = 'i')
        gam[0] = 1

        vobarg = np.zeros((self.kreg, self.kreg), order = 'F')
        vubarg = np.zeros((self.kreg, self.kreg), order = 'F')
        vbobar = self.xpy.copy()
        vbobarg = np.zeros(self.kreg)

        bubar = np.zeros(self.kreg)
        vbubar = np.dot(vubar, bubar)

        bobarg = np.zeros(self.kreg)

        #compute marginal likelihood spike slag NIG prior
        vog = t_xgxg + vubar[ind][:,ind]
        tbg = np.linalg.solve(vog, t_xgy)
        
        so = self.ypy - np.dot(t_xgy, tbg)


        ML_nig = self.compute_ML_NIG(nuo, so, vog, 
                             vubar[ind][:, ind])


        self.assertTrue((ML_nig - ML_gprior) == 0)
        


        #reset seed and draw random numbers
        np.random.seed(12345)
        ru = np.random.rand(self.kreg)
        

        #run stochastic search routine for normal inverted gamma prior
        #setup to replicate gprior

        stochsearch.ssreg_sl(vobarg, vobar, gam, vbobar, vbobarg, vubar,
                             vubarg, vbubar, bubar, self.ypy, su, bobarg,
                             nuo, ru)



        self.assertTrue(((gam-g_gamma) == 0).all())

    def compute_ML_gprior(self, qgam,  g, xgxg, xgy):
        """Function computes the marginal likelihood for g-prior
        up to a constant of proportionality."""

        c1 = -0.5* qgam * np.log(g+1)
        bog = np.linalg.solve(xgxg, xgy)
        c2 = np.dot(bog, xgy)*g / (g+1)

        return c1 - 0.5 * self.nobs * np.log((self.ypy - c2) / 2.)

    def compute_ML_NIG(self, nuo, sobar, Vobar, Vubar):
        """Function returns the marginal likelihood up to
        a constant of proportionality for the spike slab prior."""

        p1 = 0.5 * (np.log(np.linalg.det(Vubar)) - np.log(np.linalg.det(Vobar)))
        p2 = -nuo / 2. * np.log(sobar / 2)


        return p1 + p2


#Run unit tests
suite = unittest.TestLoader().loadTestsFromTestCase(TestStochsearch)
unittest.TextTestRunner(verbosity = 2).run(suite)





